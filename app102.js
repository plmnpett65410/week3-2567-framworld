function a() {
    console.log("Callback Function")
}
//Nested function
function sayHi(callback,fname, lname) {
    callback()
    function getFullName() {
        return fname + lname
    }    
    return fname + lname

}
const message = sayHi("Mr.Mark" , ("Zuckerberg"))
console.log(message)